package com.mposadar.tpagaapp;

import android.content.Context;
import android.support.test.InstrumentationRegistry;
import android.support.test.runner.AndroidJUnit4;

import com.mposadar.tpagaapp.data.TpagaRepository;
import com.mposadar.tpagaapp.data.database.VenueEntry;
import com.mposadar.tpagaapp.data.network.ApiRequests;
import com.mposadar.tpagaapp.utilities.InjectorUtils;

import org.junit.Test;
import org.junit.runner.RunWith;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

/**
 * Instrumented test, which will execute on an Android device.
 *
 * @see <a href="http://d.android.com/tools/testing">Testing documentation</a>
 */
@RunWith(AndroidJUnit4.class)
public class ExampleInstrumentedTest {
    @Test
    public void useAppContext() throws Exception {
        // Context of the app under test.
        Context appContext = InstrumentationRegistry.getTargetContext();

        assertEquals("com.mposadar.tpagaapp", appContext.getPackageName());
    }

    @Test
    public void insertVenues() {
        Context appContext = InstrumentationRegistry.getTargetContext();
        double lat = 4.710989;
        double lng = -74.072092;

        TpagaRepository repository = InjectorUtils.provideRepository(appContext);
        VenueEntry[] venues = ApiRequests.requestVenues(lat, lng);
        repository.insertVenues(venues);
        assertNotNull("", repository.getAllVenues());
    }
}
