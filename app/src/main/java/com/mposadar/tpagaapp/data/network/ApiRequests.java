package com.mposadar.tpagaapp.data.network;

import com.mposadar.tpagaapp.BuildConfig;
import com.mposadar.tpagaapp.data.database.ImageEntry;
import com.mposadar.tpagaapp.data.database.VenueEntry;
import com.mposadar.tpagaapp.data.network.model.ImageResponse;
import com.mposadar.tpagaapp.data.network.model.VenueResponse;

import java.io.IOException;
import java.util.HashMap;

import retrofit2.Call;
import retrofit2.Response;

/**
 * Created by mposadar on 16/01/18.
 */

public class ApiRequests {

    private static final TpagaService client = ServiceGenerator.createService(TpagaService.class);

    /**
     * Request the venues for the API REST
     * @param lat
     * @param lng
     * @return
     * @throws NullPointerException
     */
    public static VenueEntry[] requestVenues(double lat, double lng) throws NullPointerException {
        HashMap<String, String> params = new HashMap<>();
        params.put(ApiConstans.PARAM_CLIENT_ID, BuildConfig.CLIENT_ID);
        params.put(ApiConstans.PARAM_CLIENT_SECRET, BuildConfig.CLIENT_SECRET);
        params.put(ApiConstans.PARAM_LAT_LNG, lat + "," + lng);
        params.put(ApiConstans.PARAM_API_VERSION, ApiConstans.API_VERSION);
        params.put(ApiConstans.PARAM_RADIUS, ApiConstans.RADIUS);
        Call<VenueResponse> call = client.fetchVenues(params);
        try {
            Response<VenueResponse> response = call.execute();
            if (response.isSuccessful()) {
                return response.body().getVenues();
            }
            else {
                VenueEntry[] empty = new VenueEntry[0];
                return empty;
            }
        } catch (IOException e) {
            return null;
        }
    }

    /**
     *
     * @param venueId
     * @return
     * @throws NullPointerException
     */
    public static ImageEntry[] requestVenueImages(String venueId) throws NullPointerException {
        HashMap<String, String> params = new HashMap<>();
        params.put(ApiConstans.PARAM_CLIENT_ID, BuildConfig.CLIENT_ID);
        params.put(ApiConstans.PARAM_CLIENT_SECRET, BuildConfig.CLIENT_SECRET);
        params.put(ApiConstans.PARAM_API_VERSION, ApiConstans.API_VERSION);
        Call<ImageResponse> call = client.fetchImages(venueId, params);
        try {
            Response<ImageResponse> response = call.execute();
            if (response.isSuccessful()) {
                ImageEntry[] imageEntries = new ImageEntry[response.body().getImages().length];
                int i = 0;
                for (ImageEntry imageEntry: response.body().getImages()) {
                    ImageEntry image = new ImageEntry(imageEntry.getId(), venueId, imageEntry.getImageUrl());
                    imageEntries[i] = image;
                    i++;
                }
                return imageEntries;
            }
            else {
                return new ImageEntry[0];
            }
        } catch (IOException e) {
            return null;
        }
    }
}
