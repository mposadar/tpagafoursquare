package com.mposadar.tpagaapp.data.network.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.mposadar.tpagaapp.data.database.ImageEntry;

/**
 * Created by mposadar on 16/01/18.
 */

public class ImageResponse {
    @SerializedName("response")
    @Expose
    private MainResponse mainResponse;

    public ImageEntry[] getImages(){
        return  mainResponse.images;
    }

    public void setImages(ImageEntry[] images) {
        mainResponse.images = images;
    }

    /**
     * Class MainResponse will have a venues Array
     */
    private class MainResponse {
        private ImageEntry[] images;
    }
}
