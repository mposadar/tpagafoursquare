package com.mposadar.tpagaapp.data.database;

import android.arch.persistence.room.Database;
import android.arch.persistence.room.Room;
import android.arch.persistence.room.RoomDatabase;
import android.content.Context;

/**
 * Created by mposadar on 15/01/18.
 */

@Database(entities = {VenueEntry.class, ImageEntry.class}, version = 1)
public abstract class TpagaDatabase extends RoomDatabase {
    private static final String DATABASE_NAME = "tpaga";

    // For Singleton instantiation
    private static final Object LOCK = new Object(); // lock object to ensure thread safety.
    private static volatile TpagaDatabase sInstance;

    public static TpagaDatabase getInstance(Context context) {
        if (sInstance == null) {
            synchronized (LOCK) {
                if (sInstance == null) {
                    sInstance = Room.databaseBuilder(context.getApplicationContext(),
                            TpagaDatabase.class, TpagaDatabase.DATABASE_NAME).build();
                }
            }
        }
        return sInstance;
    }

    public abstract VenueDao venueDao();
    public abstract ImageDao imageDao();
}
