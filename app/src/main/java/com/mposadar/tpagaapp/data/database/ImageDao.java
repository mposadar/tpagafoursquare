package com.mposadar.tpagaapp.data.database;

import android.arch.lifecycle.LiveData;
import android.arch.persistence.room.Dao;
import android.arch.persistence.room.Insert;
import android.arch.persistence.room.OnConflictStrategy;
import android.arch.persistence.room.Query;

import java.util.List;

/**
 * Created by mposadar on 16/01/18.
 */
@Dao
public interface ImageDao {
    @Query("SELECT * FROM images WHERE venue = :venue")
    LiveData<List<ImageEntry>> getImage(String venue);

    // Inserts multiple venues
    @Insert(onConflict = OnConflictStrategy.REPLACE)
    void bulkInsert(ImageEntry... imageEntries);
}
