package com.mposadar.tpagaapp.data.database;

import android.arch.persistence.room.Entity;
import android.arch.persistence.room.ForeignKey;
import android.arch.persistence.room.PrimaryKey;

/**
 * Created by mposadar on 16/01/18.
 */
@Entity(tableName = "images")
public class ImageEntry {
    @PrimaryKey
    private String id;
    private String venue;
    private String imageUrl;

    public ImageEntry(String id, String venue, String imageUrl) {
        this.id = id;
        this.venue = venue;
        this.imageUrl = imageUrl;
    }

    public String getId() {
        return id;
    }

    public String getVenue() {
        return venue;
    }

    public String getImageUrl() {
        return imageUrl;
    }
}
