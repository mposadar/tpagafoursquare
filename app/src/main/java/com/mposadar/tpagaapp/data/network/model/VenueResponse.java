package com.mposadar.tpagaapp.data.network.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.mposadar.tpagaapp.data.database.VenueEntry;

import java.util.ArrayList;

/**
 * Created by mposadar on 15/01/18.
 */

public class VenueResponse {
    @SerializedName("response")
    @Expose
    private MainResponse mainResponse;

    public VenueEntry[] getVenues(){
        return  mainResponse.venues;
    }

    public void setVenues(VenueEntry[] venues) {
        mainResponse.venues = venues;
    }

    /**
     * Class MainResponse will have a venues Array
     */
    private class MainResponse {
        private VenueEntry[] venues;
    }
}
