package com.mposadar.tpagaapp.data.network;

/**
 * Created by mposadar on 16/01/18.
 */

public class ApiConstans {
    public static final String EXPLORE = "explore";
    public static final String PHOTOS = "https://api.foursquare.com/v2/venues/{venue}/photos/";
    public static final String PARAM_CLIENT_ID = "client_id";
    public static final String PARAM_CLIENT_SECRET = "client_secret";
    public static final String PARAM_LAT_LNG = "ll";
    public static final String PARAM_API_VERSION = "v";
    public static final String API_VERSION = "20170801";
    public static final String PARAM_RADIUS = "radius";
    public static final String RADIUS = "5000"; // 5km
}
