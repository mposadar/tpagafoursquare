package com.mposadar.tpagaapp.data.network;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.mposadar.tpagaapp.data.network.deserializer.ImagesDeserializer;
import com.mposadar.tpagaapp.data.network.deserializer.VenuesDeserializer;
import com.mposadar.tpagaapp.data.network.model.ImageResponse;
import com.mposadar.tpagaapp.data.network.model.VenueResponse;

import okhttp3.OkHttpClient;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

/**
 * Created by mposadar on 15/01/18.
 */
public class ServiceGenerator {
    private static final String BASE_URL = "https://api.foursquare.com/v2/venues/";
    // the builder create a rest client with the given base url
    private static Retrofit.Builder builder = new Retrofit.Builder()
            .baseUrl(BASE_URL)
            .addConverterFactory(factory());
    private static Retrofit retrofit = builder.build();
    private static OkHttpClient.Builder httpClient = new OkHttpClient.Builder();

    /**
     * The createService method takes a serviceClass, which is the annotated interface for
     * API requests, as a parameter and creates a usable client from it.
     * @param serviceClass
     * @param <S>
     * @return
     */
    public static <S> S createService(Class<S> serviceClass) {
        return retrofit.create(serviceClass);
    }

    /**
     * custom GsonConverterFactory
     * @return
     */
    private static GsonConverterFactory factory() {
        GsonBuilder gsonBuilder = new GsonBuilder()
                .registerTypeAdapter(VenueResponse.class, new VenuesDeserializer())
                .registerTypeAdapter(ImageResponse.class, new ImagesDeserializer());
        Gson gson = gsonBuilder.create();
        return GsonConverterFactory.create(gson);
    };
}
