package com.mposadar.tpagaapp.data.network;

import android.arch.lifecycle.LiveData;
import android.arch.lifecycle.MutableLiveData;
import android.content.Context;
import android.util.Log;

import com.mposadar.tpagaapp.AppExecutors;
import com.mposadar.tpagaapp.data.database.ImageEntry;
import com.mposadar.tpagaapp.data.database.VenueEntry;

/**
 * Created by mposadar on 15/01/18.
 */

public class VenueNetworkDataSource {
    private static final String LOG_TAG = VenueNetworkDataSource.class.getSimpleName();

    // For Singleton instantiation
    private static final Object LOCK = new Object();
    private static VenueNetworkDataSource sInstance;
    private final Context mContext;

    private final AppExecutors mExecutors;

    // LiveData storing the latest downloaded venues
    private MutableLiveData<VenueEntry[]> mDownloadedVenues;
    private MutableLiveData<ImageEntry[]> mDownloadedImages;

    public VenueNetworkDataSource(Context context, AppExecutors executors) {
        mContext = context;
        mExecutors = executors;
        mDownloadedVenues = new MutableLiveData<>();
        mDownloadedImages = new MutableLiveData<>();
    }

    /**
     * Get the singleton for this class
     */
    public static VenueNetworkDataSource getInstance(Context context, AppExecutors executors) {
        Log.d(LOG_TAG, "Getting the network data source");
        if (sInstance == null) {
            synchronized (LOCK) {
                sInstance = new VenueNetworkDataSource(context.getApplicationContext(), executors);
                Log.d(LOG_TAG, "Made new network data source");
            }
        }
        return sInstance;
    }

    /**
     * fetch the venues and save them in to the local database
     * @param lat
     * @param lng
     */
    public void fetchVenues(double lat, double lng) {
        Log.d(LOG_TAG, "Fetch venues started");
        mExecutors.networkIO().execute(() -> {
            VenueEntry[] venues = ApiRequests.requestVenues(lat, lng);
            mDownloadedVenues.postValue(venues);

            for (VenueEntry venueEntry: venues) {
                ImageEntry[] imageEntries = ApiRequests.requestVenueImages(venueEntry.getId());
                mDownloadedImages.postValue(imageEntries);
            }
        });
    }

    /**
     * get the venues from the LiveData Object
     * @return
     */
    public LiveData<VenueEntry[]> getVenues() {
        return mDownloadedVenues;
    }

    public LiveData<ImageEntry[]> getVenueImages() {
        return mDownloadedImages;
    }
}
