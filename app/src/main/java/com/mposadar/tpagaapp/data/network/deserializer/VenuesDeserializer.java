package com.mposadar.tpagaapp.data.network.deserializer;

import com.google.gson.Gson;
import com.google.gson.JsonArray;
import com.google.gson.JsonDeserializationContext;
import com.google.gson.JsonDeserializer;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParseException;
import com.mposadar.tpagaapp.data.database.VenueEntry;
import com.mposadar.tpagaapp.data.network.model.VenueResponse;

import java.lang.reflect.Type;
import java.util.ArrayList;

/**
 * Created by mposadar on 15/01/18.
 */
public class VenuesDeserializer implements JsonDeserializer<VenueResponse> {
    @Override
    public VenueResponse deserialize(JsonElement json, Type typeOfT, JsonDeserializationContext context) throws JsonParseException {
        Gson gson = new Gson();
        // let deserialize the json response and pass the data to VenueResponse class
        VenueResponse response = gson.fromJson(json, VenueResponse.class);
        // get the complete json object
        JsonObject venueResponseData = json.getAsJsonObject().getAsJsonObject("response");
        // get the json array of groups
        JsonArray groupsArray = venueResponseData.getAsJsonArray("groups");
        // the result we need is in the first node of groups array
        JsonArray itemsArray = groupsArray.get(0).getAsJsonObject().getAsJsonArray("items");
        // convert the jsonArray into objects of VenueResponse Class
        response.setVenues(extractVenuesFromJsonArray(itemsArray));
        return response;
    }

    /**
     * Generate the VenueEntry[] needed object
     * @param jsonArray
     * @return
     */
    private VenueEntry[] extractVenuesFromJsonArray(JsonArray jsonArray) {
        VenueEntry[] venues = new VenueEntry[jsonArray.size()];
        for (int i = 0; i < jsonArray.size(); i++) {
            JsonObject item = jsonArray.get(i).getAsJsonObject();
            JsonObject venueObject = item.get("venue").getAsJsonObject();
            String id = venueObject.get("id").getAsString();
            String name = venueObject.get("name").getAsString();
            JsonObject contact = venueObject.get("contact").getAsJsonObject();
            String phone = contact.get("phone") == null ? "No specify" : contact.get("phone").getAsString();
            JsonObject location = venueObject.get("location").getAsJsonObject();
            String address = location.get("address") == null ? "No specify" : location.get("address").getAsString();;
            String crossStreet = location.get("crossStreet") == null ? "No specify" : location.get("crossStreet").getAsString();
            double lat = location.get("lat").getAsDouble();
            double lng = location.get("lng").getAsDouble();
            int distance = location.get("distance").getAsInt();
            String message = "No specify";
            if (venueObject.get("price") != null) {
                JsonObject price = venueObject.get("price").getAsJsonObject();
                message = price.get("message").getAsString();
            }

            VenueEntry venueEntry = new VenueEntry(id, name, phone, address, crossStreet, lat, lng, distance, message);
            venues[i] = venueEntry;
        }

        return venues;
    }
}
