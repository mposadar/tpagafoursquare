package com.mposadar.tpagaapp.data.network;

import com.mposadar.tpagaapp.data.network.model.ImageResponse;
import com.mposadar.tpagaapp.data.network.model.VenueResponse;

import java.util.Map;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Path;
import retrofit2.http.QueryMap;

/**
 * Created by mposadar on 15/01/18.
 */

public interface TpagaService {
    @GET(ApiConstans.EXPLORE)
    Call<VenueResponse> fetchVenues(@QueryMap Map<String, String> options);

    @GET(ApiConstans.PHOTOS)
    Call<ImageResponse> fetchImages(@Path("venue") String venueId, @QueryMap Map<String, String> options);
}
