package com.mposadar.tpagaapp.data.database;

import android.arch.persistence.room.Entity;
import android.arch.persistence.room.Ignore;
import android.arch.persistence.room.PrimaryKey;

import java.util.List;

/**
 * Created by mposadar on 15/01/18.
 */

@Entity(tableName = "venues")
public class VenueEntry {
    @PrimaryKey
    private String id;
    private String name;
    private String phone;
    private String address;
    private String crossStreet;
    private Double lat;
    private Double lng;
    private int distance;
    private String price;
    @Ignore
    private List<ImageEntry> venueImages;

    /**
     * Constructor used by Room and VenuesDeserializer
     * @param id
     * @param name
     * @param phone
     * @param address
     * @param crossStreet
     * @param lat
     * @param lng
     * @param distance
     * @param price
     */
    public VenueEntry(String id, String name, String phone, String address, String crossStreet, Double lat, Double lng, int distance, String price) {
        this.id = id;
        this.name = name;
        this.phone = phone;
        this.address = address;
        this.crossStreet = crossStreet;
        this.lat = lat;
        this.lng = lng;
        this.distance = distance;
        this.price = price;
    }

    public String getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public String getPhone() {
        return phone;
    }

    public String getAddress() {
        return address;
    }

    public String getCrossStreet() {
        return crossStreet;
    }

    public Double getLat() {
        return lat;
    }

    public Double getLng() {
        return lng;
    }

    public int getDistance() {
        return distance;
    }

    public String getPrice() {
        return price;
    }

    public List<ImageEntry> getVenueImages() {
        return venueImages;
    }

    public void setVenueImages(List<ImageEntry> venueImages) {
        this.venueImages = venueImages;
    }
}
