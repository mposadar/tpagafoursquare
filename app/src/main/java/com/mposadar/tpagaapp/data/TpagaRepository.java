package com.mposadar.tpagaapp.data;

import android.arch.lifecycle.LiveData;
import android.util.Log;

import com.mposadar.tpagaapp.AppExecutors;
import com.mposadar.tpagaapp.data.database.ImageDao;
import com.mposadar.tpagaapp.data.database.ImageEntry;
import com.mposadar.tpagaapp.data.database.VenueDao;
import com.mposadar.tpagaapp.data.database.VenueEntry;
import com.mposadar.tpagaapp.data.network.ApiRequests;
import com.mposadar.tpagaapp.data.network.VenueNetworkDataSource;

import java.util.List;

/**
 * Created by mposadar on 16/01/18.
 */

public class TpagaRepository {

    private static final String LOG_TAG = TpagaRepository.class.getSimpleName();

    // For Singleton instantiation
    private static final Object LOCK = new Object();
    private static TpagaRepository sInstance;
    private final VenueDao mVenueDao;
    private final ImageDao mImageDao;
    private final VenueNetworkDataSource mVenueNetworkDataSource;
    private final AppExecutors mExecutors;

    public TpagaRepository(VenueDao venueDao, ImageDao imageDao, VenueNetworkDataSource venueNetworkDataSource, AppExecutors executors) {
        mVenueDao = venueDao;
        mImageDao = imageDao;
        mVenueNetworkDataSource = venueNetworkDataSource;
        mExecutors = executors;
        LiveData<VenueEntry[]> networkVenues = mVenueNetworkDataSource.getVenues();
        networkVenues.observeForever(venuesEntries -> mExecutors.diskIO().execute(() -> {
                mVenueDao.bulkInsert(venuesEntries);
            }
        ));

        LiveData<ImageEntry[]> networkImages = mVenueNetworkDataSource.getVenueImages();
        networkImages.observeForever(imageEntries -> mExecutors.diskIO().execute(() -> {
                mImageDao.bulkInsert(imageEntries);
            }
        ));
    }

    public synchronized static TpagaRepository getInstance(VenueDao venueDao, ImageDao imageDao, VenueNetworkDataSource venueNetworkDataSource,
            AppExecutors executors) {
        if (sInstance == null) {
            synchronized (LOCK) {
                sInstance = new TpagaRepository(venueDao, imageDao, venueNetworkDataSource,
                        executors);
            }
        }
        return sInstance;
    }

    /**
     * Network related operation
     */
    private void startFetchVenues(double lat, double lng) {
        mVenueNetworkDataSource.fetchVenues(lat, lng);
    }

    /**
     *
     * @return
     */
    public LiveData<List<VenueEntry>> getVenues(double lat, double lng) {
        startFetchVenues(lat, lng);
        return mVenueDao.getVenues();
    }

    public LiveData<VenueEntry> getVenue(String venueId) {
        return mVenueDao.getVenue(venueId);
    }

    public LiveData<List<ImageEntry>> getVenueImages(String venueId) {
        return mImageDao.getImage(venueId);
    }

    public void insertVenues(VenueEntry[] venueEntries) {
        mVenueDao.bulkInsert(venueEntries);
    }

    public LiveData<List<VenueEntry>> getAllVenues() {
        return mVenueDao.getVenues();
    }
}
