package com.mposadar.tpagaapp.data.database;

import android.arch.lifecycle.LiveData;
import android.arch.persistence.room.Dao;
import android.arch.persistence.room.Insert;
import android.arch.persistence.room.OnConflictStrategy;
import android.arch.persistence.room.Query;

import java.util.List;

/**
 * Created by mposadar on 15/01/18.
 */

@Dao // Required annotation for Dao to be recognized by Room
public interface VenueDao {

    @Query("SELECT * FROM venues")
    LiveData<List<VenueEntry>> getVenues();

    @Query("SELECT * FROM venues WHERE id = :id")
    LiveData<VenueEntry> getVenue(String id);

    // Inserts multiple venues
    @Insert(onConflict = OnConflictStrategy.REPLACE)
    void bulkInsert(VenueEntry... venueEntries);

}
