package com.mposadar.tpagaapp.data.network.deserializer;

import com.google.gson.Gson;
import com.google.gson.JsonArray;
import com.google.gson.JsonDeserializationContext;
import com.google.gson.JsonDeserializer;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParseException;
import com.mposadar.tpagaapp.data.database.ImageEntry;
import com.mposadar.tpagaapp.data.network.model.ImageResponse;

import java.lang.reflect.Type;

/**
 * Created by mposadar on 16/01/18.
 */
public class ImagesDeserializer implements JsonDeserializer<ImageResponse> {
    @Override
    public ImageResponse deserialize(JsonElement json, Type typeOfT, JsonDeserializationContext context) throws JsonParseException {
        Gson gson = new Gson();
        // let deserialize the json response and pass the data to VenueResponse class
        ImageResponse response = gson.fromJson(json, ImageResponse.class);
        // get the complete json object
        JsonObject responseData = json.getAsJsonObject().getAsJsonObject("response");
        JsonObject photosResponseData = responseData.getAsJsonObject().getAsJsonObject("photos");
        // get the json array of groups
        JsonArray itemsArray = photosResponseData.getAsJsonArray("items");
        // convert the jsonArray into objects of VenueResponse Class
        response.setImages(extractImagesFromJsonArray(itemsArray));
        return response;
    }

    /**
     * Generate the VenueEntry[] needed object
     * @param jsonArray
     * @return
     */
    private ImageEntry[] extractImagesFromJsonArray(JsonArray jsonArray) {
        ImageEntry[] images = new ImageEntry[jsonArray.size()];
        for (int i = 0; i < jsonArray.size(); i++) {
            JsonObject item = jsonArray.get(i).getAsJsonObject();
            String id = item.get("id").getAsString();
            String prefix = item.get("prefix").getAsString();
            String suffix = item.get("suffix").getAsString();
            String imageUrl = prefix + "100x100" + suffix;

            ImageEntry imageResponse = new ImageEntry(id, "", imageUrl);
            images[i] = imageResponse;
        }

        return images;
    }
}
