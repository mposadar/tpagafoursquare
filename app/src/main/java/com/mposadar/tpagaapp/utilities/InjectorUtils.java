package com.mposadar.tpagaapp.utilities;

import android.content.Context;

import com.mposadar.tpagaapp.AppExecutors;
import com.mposadar.tpagaapp.data.TpagaRepository;
import com.mposadar.tpagaapp.data.database.TpagaDatabase;
import com.mposadar.tpagaapp.data.network.VenueNetworkDataSource;
import com.mposadar.tpagaapp.ui.detail.VenueDetailViewModelFactory;
import com.mposadar.tpagaapp.ui.list.HomeViewModelFactory;

/**
 * Created by mposadar on 16/01/18.
 */

public class InjectorUtils {
    public static TpagaRepository provideRepository(Context context) {
        TpagaDatabase database = TpagaDatabase.getInstance(context.getApplicationContext());
        AppExecutors executors = AppExecutors.getInstance();
        VenueNetworkDataSource networkDataSource =
                VenueNetworkDataSource.getInstance(context.getApplicationContext(), executors);
        return TpagaRepository.getInstance(database.venueDao(), database.imageDao(), networkDataSource, executors);
    }

    public static VenueNetworkDataSource provideNetworkDataSource(Context context) {
        AppExecutors executors = AppExecutors.getInstance();
        return VenueNetworkDataSource.getInstance(context.getApplicationContext(), executors);
    }

    public static HomeViewModelFactory provideMainActivityViewModelFactory(Context context, double lat, double lng) {
        TpagaRepository repository = provideRepository(context.getApplicationContext());
        return new HomeViewModelFactory(repository, lat, lng);
    }

    public static VenueDetailViewModelFactory provideDetailViewModelFactory(Context context, String venueId) {
        TpagaRepository repository = provideRepository(context.getApplicationContext());
        return new VenueDetailViewModelFactory(repository, venueId);
    }
}
