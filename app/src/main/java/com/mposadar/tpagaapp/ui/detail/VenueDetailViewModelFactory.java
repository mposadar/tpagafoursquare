package com.mposadar.tpagaapp.ui.detail;

import android.arch.lifecycle.ViewModel;
import android.arch.lifecycle.ViewModelProvider;

import com.mposadar.tpagaapp.data.TpagaRepository;

/**
 * Created by mposadar on 17/01/18.
 */

public class VenueDetailViewModelFactory extends ViewModelProvider.NewInstanceFactory {
    private final TpagaRepository mRepository;
    private String venueId;

    public VenueDetailViewModelFactory(TpagaRepository repository, String venueId) {
        this.mRepository = repository;
        this.venueId = venueId;
    }

    @Override
    public <T extends ViewModel> T create(Class<T> modelClass) {
        //noinspection unchecked
        return (T) new VenueDetailViewModel(mRepository, venueId);
    }
}
