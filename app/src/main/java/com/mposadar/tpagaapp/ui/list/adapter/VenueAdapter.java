package com.mposadar.tpagaapp.ui.list.adapter;

import android.arch.lifecycle.LifecycleOwner;
import android.arch.lifecycle.LiveData;
import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.mikhaellopez.circularimageview.CircularImageView;
import com.mposadar.tpagaapp.R;
import com.mposadar.tpagaapp.data.TpagaRepository;
import com.mposadar.tpagaapp.data.database.ImageEntry;
import com.mposadar.tpagaapp.data.database.VenueEntry;
import com.mposadar.tpagaapp.ui.detail.VenueDetailActivity;
import com.mposadar.tpagaapp.utilities.InjectorUtils;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by mposadar on 16/01/18.
 */

public class VenueAdapter extends RecyclerView.Adapter<VenueAdapter.VenueViewHolder> {

    private Context mContext;
    private TpagaRepository mRepository;
    private List<VenueEntry> mVenueEntries;

    public VenueAdapter(Context context) {
        this.mContext = context;
        mVenueEntries = new ArrayList<>();
        mRepository = InjectorUtils.provideRepository(mContext);
    }

    @Override
    public VenueViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(mContext).inflate(R.layout.list_item_venue, parent, false);
        return new VenueViewHolder(view);
    }

    @Override
    public void onBindViewHolder(VenueViewHolder holder, int position) {
        VenueEntry venueEntry = mVenueEntries.get(position);
        holder.mNameTextView.setText(venueEntry.getName());
        String address;
        if (venueEntry.getCrossStreet().equals("No specify")) {
        address = venueEntry.getAddress();
        }
        else {
         address = venueEntry.getAddress() + " | " + venueEntry.getCrossStreet();
        }
        holder.mAddressTextView.setText(address);
        String distance;
        if (venueEntry.getDistance() > 999) {
            distance = Math.round(venueEntry.getDistance() / 1000.0d)  + " KM";
        }
        else {
            distance = venueEntry.getDistance() + " M";
        }
        holder.mDistanceToVenueTextView.setText(distance);

        LiveData<List<ImageEntry>> images = mRepository.getVenueImages(venueEntry.getId());
        images.observe((LifecycleOwner) mContext, imageEntries -> {
            if (imageEntries.size() > 0) {
                Picasso.with(mContext).load(imageEntries.get(0).getImageUrl()).into(holder.mImageView);
            }
        });
    }

    @Override
    public int getItemCount() {
        return mVenueEntries == null ? 0 : mVenueEntries.size();
    }

    /**
     * Swaps the list used by the VenueAdapter for its weather data
     * @param newVenueEntries
     */
    public void swapVenues(List<VenueEntry> newVenueEntries) {
        if (mVenueEntries != null) {
            mVenueEntries.clear();
        }
        mVenueEntries.addAll(newVenueEntries);
        notifyDataSetChanged();
    }

    class VenueViewHolder extends RecyclerView.ViewHolder {

        CircularImageView mImageView;
        TextView mNameTextView;
        TextView mAddressTextView;
        TextView mDistanceToVenueTextView;

        public VenueViewHolder(View itemView) {
            super(itemView);

            mImageView = itemView.findViewById(R.id.venueImageView);
            mNameTextView = itemView.findViewById(R.id.venueNameTextView);
            mAddressTextView = itemView.findViewById(R.id.venueAddressTextView);
            mDistanceToVenueTextView = itemView.findViewById(R.id.distanceToVenueTextView);

            itemView.setOnClickListener(view -> {
                VenueEntry venueEntry = mVenueEntries.get(getPosition());
                // Log.e("click", "position: " + getPosition() + ", " + venueEntry.getName());
                Intent intent = new Intent(mContext, VenueDetailActivity.class);
                intent.putExtra("venueId", venueEntry.getId());
                mContext.startActivity(intent);
            });
        }
    }
}
