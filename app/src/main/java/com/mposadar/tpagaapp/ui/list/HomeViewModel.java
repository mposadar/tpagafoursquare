package com.mposadar.tpagaapp.ui.list;

import android.arch.lifecycle.LiveData;
import android.arch.lifecycle.MutableLiveData;
import android.arch.lifecycle.ViewModel;

import com.mposadar.tpagaapp.data.TpagaRepository;
import com.mposadar.tpagaapp.data.database.ImageEntry;
import com.mposadar.tpagaapp.data.database.VenueEntry;

import java.util.List;

/**
 * {@Link ViewModel} for {@Link HomeActivity}
 */

public class HomeViewModel extends ViewModel {

    private final TpagaRepository mRepository;
    private final LiveData<List<VenueEntry>> mVenues;

    public HomeViewModel(TpagaRepository repository, double lat, double lng) {
        mRepository = repository;
        mVenues = mRepository.getVenues(lat, lng);
    }

    public LiveData<List<VenueEntry>> getVenues() {
        return mVenues;
    }
}
