package com.mposadar.tpagaapp.ui.detail;

import android.Manifest;
import android.arch.lifecycle.LifecycleOwner;
import android.arch.lifecycle.LiveData;
import android.arch.lifecycle.ViewModelProviders;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.TextView;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapView;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.UiSettings;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;
import com.mposadar.tpagaapp.R;
import com.mposadar.tpagaapp.data.database.ImageEntry;
import com.mposadar.tpagaapp.ui.detail.adapter.VenueImageAdapter;
import com.mposadar.tpagaapp.utilities.InjectorUtils;
import com.squareup.picasso.Picasso;

import java.util.List;

public class VenueDetailActivity extends AppCompatActivity implements OnMapReadyCallback {

    private Toolbar toolbar;
    private GoogleMap mGoogleMap;
    private static final int REQUEST_CODE = 200;
    private static final String BUNDLE_KEY = "DETAIL_MAP";
    private String venueId;
    private VenueDetailViewModel mViewModel;
    private VenueImageAdapter mAdapter;

    // UI
    private MapView mMapView;
    private TextView nameTextView, addressTextView, phonetextView, distanceTextView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_venue_detail);
        toolbar = findViewById(R.id.toolbar);
        toolbar.setTitle("Home");
        setSupportActionBar(toolbar);
        ActionBar ab = getSupportActionBar();
        ab.setDisplayHomeAsUpEnabled(true);

        Bundle bundle = getIntent().getExtras();
        venueId = bundle.getString("venueId");

        mMapView = findViewById(R.id.detailMap);
        nameTextView = findViewById(R.id.nameTextView);
        addressTextView = findViewById(R.id.addressTextView);
        phonetextView = findViewById(R.id.phoneTextView);
        distanceTextView = findViewById(R.id.distanceTextView);

        Bundle mapViewBundle = null;
        if (savedInstanceState != null) {
            mapViewBundle = savedInstanceState.getBundle(BUNDLE_KEY);
        }
        mMapView.onCreate(mapViewBundle);
        mMapView.getMapAsync(this);

        VenueDetailViewModelFactory factory = InjectorUtils.provideDetailViewModelFactory(this, venueId);
        mViewModel = ViewModelProviders.of(this, factory).get(VenueDetailViewModel.class);
        mViewModel.getVenue().observe(this, venue -> {
            LatLng latLng = new LatLng(venue.getLat(), venue.getLng());
            mGoogleMap.moveCamera(CameraUpdateFactory.newLatLng(latLng));
            mGoogleMap.addMarker(new MarkerOptions().position(latLng).title(venue.getName()));

            nameTextView.setText(venue.getName());
            addressTextView.setText(venue.getAddress() + " | " + venue.getCrossStreet());
            phonetextView.setText(venue.getPhone());
            distanceTextView.setText((venue.getDistance() / 1000.0d) + " km");
        });

        RecyclerView mRecyclerView = findViewById(R.id.recyclerView);
        LinearLayoutManager layoutManager =
                new LinearLayoutManager(this, LinearLayoutManager.HORIZONTAL, false);
        mRecyclerView.setLayoutManager(layoutManager);
        mRecyclerView.setHasFixedSize(true);

        mAdapter = new VenueImageAdapter(this);
        mRecyclerView.setAdapter(mAdapter);
        mViewModel.getVenueImages().observe(this, images -> {
            mAdapter.swapVenuesImages(images);
        });
    }

    private void setupMap(GoogleMap googleMap) {
        mGoogleMap = googleMap;
        mGoogleMap.setMinZoomPreference(10);
        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.ACCESS_FINE_LOCATION, Manifest.permission.ACCESS_COARSE_LOCATION}, REQUEST_CODE);
        }
        else {
            mGoogleMap.setMyLocationEnabled(true);
        }
        UiSettings settings = mGoogleMap.getUiSettings();
        settings.setZoomControlsEnabled(true);
        settings.setAllGesturesEnabled(true);
    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        setupMap(googleMap);
    }

    @Override
    protected void onResume() {
        super.onResume();
        mMapView.onResume();
    }

    @Override
    protected void onStart() {
        super.onStart();
        mMapView.onStart();
    }

    @Override
    protected void onStop() {
        super.onStop();
        mMapView.onStop();
    }
    @Override
    protected void onPause() {
        mMapView.onPause();
        super.onPause();
    }
    @Override
    protected void onDestroy() {
        mMapView.onDestroy();
        super.onDestroy();
    }
    @Override
    public void onLowMemory() {
        super.onLowMemory();
        mMapView.onLowMemory();
    }
}
