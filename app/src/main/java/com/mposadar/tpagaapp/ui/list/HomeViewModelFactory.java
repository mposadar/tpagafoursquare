package com.mposadar.tpagaapp.ui.list;

import android.arch.lifecycle.ViewModel;
import android.arch.lifecycle.ViewModelProvider;

import com.mposadar.tpagaapp.data.TpagaRepository;

/**
 * Created by mposadar on 16/01/18.
 */

public class HomeViewModelFactory extends ViewModelProvider.NewInstanceFactory {
    private final TpagaRepository mRepository;
    private double lat;
    private double lng;

    public HomeViewModelFactory(TpagaRepository repository, double lat, double lng) {
        this.mRepository = repository;
        this.lat = lat;
        this.lng = lng;
    }

    @Override
    public <T extends ViewModel> T create(Class<T> modelClass) {
        //noinspection unchecked
        return (T) new HomeViewModel(mRepository, lat, lng);
    }
}
