package com.mposadar.tpagaapp.ui.list;

import android.arch.lifecycle.ViewModelProviders;
import android.location.Location;
import android.support.v4.app.Fragment;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.mposadar.tpagaapp.R;
import com.mposadar.tpagaapp.data.database.ImageEntry;
import com.mposadar.tpagaapp.data.database.VenueEntry;
import com.mposadar.tpagaapp.ui.list.adapter.VenueAdapter;
import com.mposadar.tpagaapp.utilities.InjectorUtils;

import java.util.List;

/**
 * A placeholder fragment containing a simple view.
 */
public class VenueListFragment extends Fragment {

    private HomeViewModel mViewModel;
    private VenueAdapter mVenueAdapter;
    private RecyclerView mRecyclerView;

    public VenueListFragment() {
    }

    public static VenueListFragment newInstance(double lat, double lng) {
        VenueListFragment fragment = new VenueListFragment();
        Bundle arguments = new Bundle();
        arguments.putDouble("lat", lat);
        arguments.putDouble("lng", lng);
        fragment.setArguments(arguments);
        return fragment;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        Bundle arguments = getArguments();
        double lat = arguments.getDouble("lat");
        double lng = arguments.getDouble("lng");

        View view = inflater.inflate(R.layout.fragment_venue_list, container, false);

        mRecyclerView = view.findViewById(R.id.venueRecyclerView);
        LinearLayoutManager layoutManager =
                new LinearLayoutManager(getActivity(), LinearLayoutManager.VERTICAL, false);
        mRecyclerView.setLayoutManager(layoutManager);
        mRecyclerView.setHasFixedSize(true);

        mVenueAdapter = new VenueAdapter(getActivity());
        // mVenueAdapter.setHasStableIds(true);
        mRecyclerView.setAdapter(mVenueAdapter);

        HomeViewModelFactory factory = InjectorUtils.provideMainActivityViewModelFactory(getActivity(), lat, lng);
        mViewModel = ViewModelProviders.of(getActivity(), factory).get(HomeViewModel.class);
        mViewModel.getVenues().observe(getActivity(), venueEntries -> {
            // add to adapter ...
            // List<ImageEntry> imageEntries = mViewModel
            mVenueAdapter.swapVenues(venueEntries);
        });
        return view;
    }
}
