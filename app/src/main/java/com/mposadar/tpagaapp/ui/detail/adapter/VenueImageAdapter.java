package com.mposadar.tpagaapp.ui.detail.adapter;

import android.arch.lifecycle.LifecycleOwner;
import android.arch.lifecycle.LiveData;
import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.mikhaellopez.circularimageview.CircularImageView;
import com.mposadar.tpagaapp.R;
import com.mposadar.tpagaapp.data.TpagaRepository;
import com.mposadar.tpagaapp.data.database.ImageEntry;
import com.mposadar.tpagaapp.data.database.VenueEntry;
import com.mposadar.tpagaapp.ui.detail.VenueDetailActivity;
import com.mposadar.tpagaapp.utilities.InjectorUtils;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by mposadar on 16/01/18.
 */

public class VenueImageAdapter extends RecyclerView.Adapter<VenueImageAdapter.VenueImageViewHolder> {

    private Context mContext;
    private List<ImageEntry> mImageEntries;

    public VenueImageAdapter(Context context) {
        this.mContext = context;
        mImageEntries = new ArrayList<>();
    }

    @Override
    public VenueImageViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(mContext).inflate(R.layout.list_item_photos, parent, false);
        return new VenueImageViewHolder(view);
    }

    @Override
    public void onBindViewHolder(VenueImageViewHolder holder, int position) {
        ImageEntry imageEntry = mImageEntries.get(position);
        Picasso.with(mContext).load(imageEntry.getImageUrl()).into(holder.mImageView);
    }

    @Override
    public int getItemCount() {
        return mImageEntries == null ? 0 : mImageEntries.size();
    }

    public void swapVenuesImages(List<ImageEntry> newImageEntries) {
        if (mImageEntries != null) {
            mImageEntries.clear();
        }
        mImageEntries.addAll(newImageEntries);
        notifyDataSetChanged();
    }

    class VenueImageViewHolder extends RecyclerView.ViewHolder {
        ImageView mImageView;
        public VenueImageViewHolder(View itemView) {
            super(itemView);
            mImageView = itemView.findViewById(R.id.venueImageView);
        }
    }
}
