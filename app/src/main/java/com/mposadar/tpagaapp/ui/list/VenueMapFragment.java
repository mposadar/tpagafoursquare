package com.mposadar.tpagaapp.ui.list;


import android.Manifest;
import android.annotation.SuppressLint;
import android.arch.lifecycle.ViewModelProviders;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapView;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.UiSettings;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;
import com.mposadar.tpagaapp.R;
import com.mposadar.tpagaapp.data.database.VenueEntry;
import com.mposadar.tpagaapp.utilities.InjectorUtils;

/**
 * Created by mposadar on 16/01/18.
 */

public class VenueMapFragment extends Fragment implements OnMapReadyCallback {

    private static final String BUNDLE_KEY = "MAP";
    private HomeViewModel mViewModel;
    private static final int REQUEST_CODE = 200;
    private double lat;
    private double lng;

    private MapView mMapView;
    private GoogleMap mGoogleMap;

    public VenueMapFragment() {
    }

    public static VenueMapFragment newInstance(double lat, double lng) {
        VenueMapFragment fragment = new VenueMapFragment();
        Bundle arguments = new Bundle();
        arguments.putDouble("lat", lat);
        arguments.putDouble("lng", lng);
        fragment.setArguments(arguments);
        return fragment;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {

        Bundle arguments = getArguments();
        lat = arguments.getDouble("lat");
        lng = arguments.getDouble("lng");

        View view = inflater.inflate(R.layout.fragment_venue_map, container, false);
        mMapView = view.findViewById(R.id.mapView);

        Bundle mapViewBundle = null;
        if (savedInstanceState != null) {
            mapViewBundle = savedInstanceState.getBundle(BUNDLE_KEY);
        }
        mMapView.onCreate(mapViewBundle);
        mMapView.getMapAsync(this);

        return view;
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        Bundle mapViewBundle = outState.getBundle(BUNDLE_KEY);
        if (mapViewBundle == null) {
            mapViewBundle = new Bundle();
            outState.putBundle(BUNDLE_KEY, mapViewBundle);
        }
        mMapView.onSaveInstanceState(mapViewBundle);
    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        setupMap(googleMap);

        LatLng latLng = new LatLng(lat, lng);
        mGoogleMap.moveCamera(CameraUpdateFactory.newLatLng(latLng));

        HomeViewModelFactory factory = InjectorUtils.provideMainActivityViewModelFactory(getActivity(), lat, lng);
        mViewModel = ViewModelProviders.of(getActivity(), factory).get(HomeViewModel.class);
        mViewModel.getVenues().observe(getActivity(), venueEntries -> {
            // add to adapter ...
            for (VenueEntry venueEntry: venueEntries) {
                LatLng mk = new LatLng(venueEntry.getLat(), venueEntry.getLng());
                mGoogleMap.addMarker(new MarkerOptions().position(mk).title(venueEntry.getName()));
            }
        });
    }

    private void setupMap(GoogleMap googleMap) {
        mGoogleMap = googleMap;
        mGoogleMap.setMinZoomPreference(10);
        if (ActivityCompat.checkSelfPermission(getActivity(), Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(getActivity(), Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            requestPermissions(new String[]{Manifest.permission.ACCESS_FINE_LOCATION, Manifest.permission.ACCESS_COARSE_LOCATION}, REQUEST_CODE);
        }
        else {
            mGoogleMap.setMyLocationEnabled(true);
        }
        UiSettings settings = mGoogleMap.getUiSettings();
        settings.setZoomControlsEnabled(true);
        settings.setAllGesturesEnabled(true);
    }

    @SuppressLint("MissingPermission")
    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        mGoogleMap.setMyLocationEnabled(true);
    }

    @Override
    public void onResume() {
        super.onResume();
        mMapView.onResume();
    }

    @Override
    public void onStart() {
        super.onStart();
        mMapView.onStart();
    }

    @Override
    public void onStop() {
        super.onStop();
        mMapView.onStop();
    }
    @Override
    public void onPause() {
        mMapView.onPause();
        super.onPause();
    }
    @Override
    public void onDestroy() {
        mMapView.onDestroy();
        super.onDestroy();
    }
    @Override
    public void onLowMemory() {
        super.onLowMemory();
        mMapView.onLowMemory();
    }
}
