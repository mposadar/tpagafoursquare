package com.mposadar.tpagaapp.ui.detail;

import android.arch.lifecycle.LiveData;
import android.arch.lifecycle.ViewModel;

import com.mposadar.tpagaapp.data.TpagaRepository;
import com.mposadar.tpagaapp.data.database.ImageEntry;
import com.mposadar.tpagaapp.data.database.VenueEntry;

import java.util.List;

/**
 * Created by mposadar on 17/01/18.
 */

public class VenueDetailViewModel extends ViewModel {
    private final TpagaRepository mRepository;
    private final LiveData<VenueEntry> mVenue;
    private final LiveData<List<ImageEntry>> mImages;

    public VenueDetailViewModel(TpagaRepository repository, String venueId) {
        mRepository = repository;
        mVenue = mRepository.getVenue(venueId);
        mImages = mRepository.getVenueImages(venueId);
    }

    public LiveData<VenueEntry> getVenue() {
        return mVenue;
    }
    public LiveData<List<ImageEntry>> getVenueImages() {
        return mImages;
    }
}
