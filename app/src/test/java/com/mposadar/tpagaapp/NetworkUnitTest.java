package com.mposadar.tpagaapp;

import com.mposadar.tpagaapp.data.database.ImageEntry;
import com.mposadar.tpagaapp.data.database.VenueEntry;
import com.mposadar.tpagaapp.data.network.ApiRequests;

import org.junit.Test;

import static org.junit.Assert.*;

/**
 * Example local unit test, which will execute on the development machine (host).
 *
 * @see <a href="http://d.android.com/tools/testing">Testing documentation</a>
 */
public class NetworkUnitTest {

    /**
     * The request should return VenueEntry[]
     * @throws Exception
     */
    @Test
    public void requestVenues() throws Exception {
        double lat = 4.710989;
        double lng = -74.072092;
        VenueEntry[] venues = ApiRequests.requestVenues(lat, lng);
        assertNotNull(venues);
    }

    /**
     * The request should return ImageEntry[]
     * @throws Exception
     */
    @Test
    public void requestImages() throws Exception {
        String venueId = "4d7186ac4ab5224b75d4b097";
        ImageEntry[] venues = ApiRequests.requestVenueImages(venueId);
        assertNotNull(venues);
    }
}