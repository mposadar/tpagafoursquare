# Tpaga Test App

## The Requirements

* Display a 3 column grid list or a simple list of items, each list item must have at
least one image, 2 relevant details of each venue and distance to it. List items
must be sorted by distance within a radius of 5 km (you can choose the better
radius for this app).
* Display a Map centered on the current gps location and corresponding venue
marker overlays sorted using the same sorting requirement as with grid list.
* Display a detail view of selected item, with an interesting design you would like to
do about the venue.

## Build with

### Android Architecture Components

A collection of libraries that help you design robust, testable, and maintainable apps.
Start with classes for managing your UI component lifecycle and handling data persistence.

* [LiveData](https://developer.android.com/topic/libraries/architecture/livedata.html) - An observable data holder class. Unlike a regular observable, LiveData is lifecycle-aware.
* [ViewModel](https://developer.android.com/topic/libraries/architecture/viewmodel.html) - The ViewModel class allows data to survive configuration changes such as screen rotations.
* [Room](https://developer.android.com/topic/libraries/architecture/room.html) - The Room persistence library provides an abstraction layer over SQLite to allow fluent database access while harnessing the full power of SQLite.

### Libs

* [Retrofit](http://square.github.io/retrofit/) - A type-safe HTTP client for Android and Java.
* [Picasso](http://square.github.io/picasso/) - A powerful image downloading and caching library for Android.

## Unit Test and Instrumented Test

* Test that the requests methods works properly
* Test that the local database save data and return them after insert.


